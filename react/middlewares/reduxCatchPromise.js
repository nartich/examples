'use strict';

export default function(callback) {
    return (store) => (next) => (action) => {

        let result = next(action);

        const isPromise = (typeof result === 'object' && typeof result.then === 'function');

        if (isPromise && typeof callback === 'function') {
            callback(result, action, store);
        }

        return result;
    };
}
