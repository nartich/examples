/**
 *
 */

'use strict';

import React from 'react';
import {connect} from 'react-redux';
import {addLocaleData, IntlProvider,intlShape} from 'react-intl';
import langMessages from '../lang';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// project
import * as SysActions     from '../actions/sysActions.js';
import * as CatalogActions from '../actions/catalogActions.js';
import * as AuthActions    from '../actions/authActions2.js';
import * as ServiceActions from '../actions/serviceActions';
import * as DirectoriesActions from '../actions/directoriesActions';
import * as CatalogIcons   from '../components/svgicons.js';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';

function getLang()
{
    let navigator = global.navigator?global.navigator:undefined;
    if (navigator && navigator.languages != undefined)
        return navigator.languages[0];
    else
        return navigator && navigator.language || 'en';
}
const defaultLocale = 'en';
var   systemLocale  = 'en';
var   localeProp    = 'en';
var    messages      = [];

var App = React.createClass({

    childContextTypes: {
        handleOpenMenu: React.PropTypes.func.isRequired,
        handleChangeMenu: React.PropTypes.func.isRequired,
        closeAll: React.PropTypes.func.isRequired,
        authDialogOpen: React.PropTypes.func.isRequired
    },

    getChildContext: function () {
        return {
            handleOpenMenu: this.handleOpenMenu,
            handleChangeMenu: this.handleChangeMenu,
            closeAll: this.closeAll,
            authDialogOpen: this.handleAuthDialogOpen,
        };
    },

    contextTypes: {
        router: React.PropTypes.object.isRequired

    },

    preloaderDisable: function () {

        let preloader = document.getElementById('preloader');
        if (preloader) preloader.style.display = 'none';

    },

    preloaderEnable: function () {

        let preloader = document.getElementById('preloader');
        if (preloader) preloader.style.display = 'block';
    },

    getInitialState: function () {
        return {
            windowWidth:  global.window ? global.window.innerWidth:0,
            windowHeight: global.window ? global.window.innerHeight:0,
            openMenu: false,
            openAuthDialog: false
        };
    },

    componentWillReceiveProps(nextProps){

        if (nextProps.inProgressTotal) {
            this.preloaderEnable()
        }
        else {
            if (nextProps.rehydrateComplete && !this.props.rehydrateComplete && !this.props.isServer) {
                    this.startActions();
            }
        }


        if ( nextProps.catalog ) {
            this.setState ( {catalog: Object.values(nextProps.catalog) });
        }
    },

    onPressLenta: function () {
        this.context.router.push('/ribbon');
    },


    onPressCatalog: function () {
        this.closeAll({openAuthDialog: false});
        this.context.router.push('/catalog');
    },

    onPressSearch: function () {
        this.closeAll({openAuthDialog: false});
        this.context.router.push('/advsearch');
    },

    onPressAuth: function ( ) {

        this.closeAll({openAuthDialog: false});
        this.context.router.push( { pathname: '/auth' } );
    },

    onPressReg: function ( ) {

        this.closeAll({openAuthDialog: false});
        this.context.router.push( { pathname: '/auth/reg' } );
    },

    handleOpenMenu: function () {
        this.setState({openMenu: true});
    },

    handleChangeMenu: function () {
        this.setState({openMenu: false});
    },

    startActions: function () {

        this.props.getAllDirectories();
        this.props.getCatalog();

        return this.props.servicesPopular();
    },

    mainHandleResize: function (e) {
        let windowData  = { windowWidth:  global.window ? global.window.innerWidth:0,
                            windowHeight: global.window ? global.window.innerHeight:0 };

        this.props.windowResizeHandler( windowData );

        let nState = windowData;
        //console.log('ref,',this.refs.mainContainer);
        if (this.refs && this.refs.mainContainer && this.refs.mainContainer.offsetHeight) {
            nState = {...nState, mainContainerHeight: this.refs.mainContainer.offsetHeight};
        }

        this.setState(nState);
    },

    componentWillUnmount: function () {

        if ( global.window ) global.window.removeEventListener('resize', this.mainHandleResize);
    },


    componentWillMount: function () {
        if ( this.props.isServer && !this.props.params.isSecondRender ){
            this.startActions();
        }
        else if ( global.window ) global.window.addEventListener('resize', this.mainHandleResize);
    },

    componentDidMount: function () {

        let windowData  = { windowWidth:  global.window ? global.window.innerWidth:0,
                            windowHeight: global.window ? global.window.innerHeight:0 };

        this.props.windowResizeHandler( windowData );

        if (!this.props.inProgressTotal) {
            this.preloaderDisable();

            if (this.props.rehydrateComplete) {
                this.startActions();
            }
        }

        let nState = windowData;
        if (this.refs && this.refs.mainContainer && this.refs.mainContainer.offsetHeight) {
            nState = {...nState, mainContainerHeight: this.refs.mainContainer.offsetHeight};
        }
        this.setState(nState);
    },

    handleAuthDialogOpen: function () {
        this.setState({openAuthDialog: true});
    },

    handleAuthDialogClose: function () {
        this.setState({openAuthDialog: false});
    },

    onPressMenuItem: function( url ){
        this.context.router.push( { pathname: '/'+url } )
    },


    onPressStatic: function( page ){
        this.props.getInfoPage( page );
        this.onPressMenuItem ( 'static/'+page );
    },

    onLogoutPress: function(){
        this.props.doLogout();
        this.context.router.push( { pathname: '/' } )
    },

    catalogFoward: function (e) {
        e.stopPropagation();
        let catalog = this.state.catalog;
        let obj = catalog.splice(0,1);
        catalog.push(obj[0]);
        this.setState({catalog});
    },

    catalogBack: function ( e ) {
        e.stopPropagation();
        let catalog = this.state.catalog;
        let obj = catalog.splice(catalog.length-1,1);
        catalog.unshift(obj[0]);
        this.setState({catalog});
    },

    pushAdvSearch(id, keyword )
    {
        if (id) {
                let c = this.props.catalog[id];
                this.props.serviceFilter('', {catalog_id: c.childs});
                this.context.router.push({pathname: '/advsearch/' + id});
        }
        else if (keyword)
        {
            this.props.serviceFilter(keyword, {});
            this.context.router.push({pathname: '/searchstr/' + encodeURIComponent(keyword)});
        }
        else this.context.router.push({pathname: '/advsearch/'});

    },

    getUserLocale ()
    {
        return  this.props.userData && this.props.userData.user &&
                this.props.userData.user.language &&
                this.props.userData.user.language.alias ? this.props.userData.user.language.alias : null ;
    },

    getLocale() {

        systemLocale   = getLang();
        let userLocale = this.getUserLocale();
       
        localeProp = userLocale || this.props.lang || systemLocale;

        messages = langMessages[localeProp.split('-')[0]] || langMessages[systemLocale.split('-')[0]] || langMessages[defaultLocale];
    },


    render() {

        this.getLocale();

        const authActions = [
            <FlatButton
                label={messages['common.cancel.uppercase']}
                primary={true}
                onClick={this.handleAuthDialogClose}
            />,
            <FlatButton
                label={messages['auth.authorization.uppercase']}
                primary={true}
                onClick={this.onPressAuth}
            />,
        ];

        let content = this.props.children;

        let Logo = CatalogIcons["logo"];

        let contentStyle = {}

        let header_picture = global.location && global.location.pathname=='/' && this.props.rehydrateComplete && !this.props.isAuth?"nd_header_picture":"nd_header_no_picture";
        let header_color   = global.location && global.location.pathname=='/' && this.props.rehydrateComplete && !this.props.isAuth?"nd_header_transparent":"";
        let catalog = this.state.catalog || [];

        let catalog_list = [];
        if (catalog && catalog.length > 0) {
            for (let i = 0; i < ( this.props.windowWidth && this.props.windowWidth < 401 ? catalog.length : 6 ) ; i++)
            {

            let el = catalog[i];
            if (el) {
                let btn = i == 0 ? <div id="nd_catalog_back_btn" onClick={this.catalogBack}></div> :
                    i > 0 && i < 5 ?
                        <div id={"nd_catalog_foward_btn" + (i + 1)} onClick={this.catalogFoward}></div> : null;
 
                catalog_list.push(<div className="nd_catalog_item_container" id={"catalog" + (i + 1)} key={"catalog_"+i}
                                       onClick={() => this.pushAdvSearch(el.id, el.keyword)}>
                    <div className="nd_catalog_item_wrapper">
                        <div className="nd_catalog_item"
                             style={{backgroundImage: 'url(' + (el.img ? el.img : '/images/' + el.icon_name + '.jpg)')}}>
                            <div className="nd_catalog_text_container">
                                <div className="nd_catalog_text">{el.name}</div>
                            </div>
                            {btn}
                        </div>
                    </div>
                </div>);
            }
           }
        }
        return ( this.props.inProgressTotal?<div></div>:
            <IntlProvider locale={localeProp} key={localeProp} messages={messages} defaultLocale={defaultLocale}>
                <MuiThemeProvider muiTheme={getMuiTheme()} style={{alignSelf: 'center', width: '100%'}}>
                    <div className="nd_root">
                        <div className={'nd_header_container '+header_picture}>
                            <div className={'nd_header '+header_color}>
                                <div className='nd_header_line'>
                                    <div className='nd_header_left_container'>
                                        <div className="nd_logo">
                                            <div className="nd_logo_onclick" onClick={()=> this.context.router.push( { pathname: '/' } )}/>
                                            <div className="nd_logo_phone">
                                                <a  href="tel://8-800-550-14-38">8-800-550-14-38</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={"nd_header_right_container" +
                                                      (this.props.isAuth?"":" nd_header_right_container_noauth")}>
                                        <div className="nd_header_menu_titles">
                                            <div className="nd_header_menu_title"><a onClick={()=>this.onPressStatic('about')}>{messages['index.about']}</a></div>
                                            <div className="nd_header_menu_title" id="nd_header_menu_login"><a onClick={()=>this.context.router.push( { pathname: '/auth' } )}>{messages['index.login']}</a></div>
                                            <div className="nd_header_menu_title"><a onClick={this.onPressSearch}>{messages['index.search']}</a></div>
                                            <div className="nd_header_menu_title" id="nd_header_menu_help"><a onClick={()=>this.onPressStatic('help')}>{messages['index.help']}</a></div>
                                        </div>
                                        <div className="nd_header_avatar_container">
                                            <div className="nd_header_searchico"><a onClick={this.onPressSearch}><img src="/images/searchico.png" style={{margin: 1}}/></a></div>
                                            <IconMenu style={{alignSelf:'center'}}
                                                      iconButtonElement={<IconButton>
                                                          <MenuIcon /></IconButton>}
                                                      anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                                                      targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                                                <MenuItem primaryText={messages['header.menu.feed.uppercase']}
                                                          onClick={()=>this.onPressMenuItem('ribbon')}/>
                                                <MenuItem className='nd_menu_nonwide_element'
                                                          primaryText={messages['header.menu.help.uppercase']}
                                                          onClick={()=>this.onPressStatic('help')}/>
                                                {this.props.isAuth?<MenuItem primaryText={messages['common.logout.uppercase']}  onClick={this.onLogoutPress} />:
                                                    <MenuItem primaryText={messages['auth.signup.uppercase']}                   onClick={()=>this.onPressMenuItem('auth/reg')} />}
                                            </IconMenu>
                                            {this.props.rehydrateComplete && !this.props.isAuth ? null :
                                                <img src={this.props.userData && this.props.userData.user && this.props.userData.user.userpic_thumbnail ?
                                                    this.props.userData.user.userpic_thumbnail : "/images/avatar.png"} className="nd_header_avatar"
                                                     onClick={()=> this.context.router.push( { pathname: '/profile/' } )}
                                                />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="nd_header_info_container" style={global.location && global.location.pathname=='/' && this.props.rehydrateComplete && !this.props.isAuth?{}:{display:'none'}}>
                                <div className="nd_header_info">
                                    <div className="nd_header_text">Активный отдых в России</div>
                                </div>
                            </div>
                        </div>
                        <div className={ (global.location && global.location.pathname=='/' && this.props.rehydrateComplete && !this.props.isAuth ?"nd_main_container":"nd_main_container_nopic") +
                                         (global.location && global.location.pathname.indexOf('/advsearch')>-1 || global.location && global.location.pathname.indexOf('/searchstr')>-1 ?' nd_main_search_container':'')

                        }>
                            <div className="nd_main" ref="mainContainer">
                                {global.location && global.location.pathname=='/'?<div className="nd_main_catalog_title">
                                        <h1>{messages['index.directions']  /* Направления */ }</h1>
                                    </div>:null}
                                {global.location && global.location.pathname=='/'?<div className="nd_catalog_container">
                                        {catalog_list}
                                    </div>:null}
                                <div className='nd_content_container'>
                                    <div className='nd_content' onClick={this.closeAll}>
                                       {content}
                                    </div>
                                </div>
                                <div className='nd_footer_container'>
                                    <div className='nd_footer_top'>
                                        <div className='nd_footer_content'>
                                            <div className="nd_footer_links">
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('official')}      >{messages['index.official']}</a></div>
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('paymethods')}    ><img src="https://coinshome.site/images/bitcoin.png" style={{width:18, height:18, verticalAlign: 'middle'}}/> {messages['index.paymethods']}</a></div>
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('usercondition')} >{messages['index.user.agreement']}</a></div>
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('sellerrules')}   >{messages['index.seller.rules']}</a></div>
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('about')}         >{messages['index.about']}</a></div>
                                            <div className='nd_footer_link'><a onClick={()=>this.onPressStatic('cooperation')}   >{messages['index.cooperation']}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='nd_footer_middle'>
                                        <div className='nd_footer_middle_content'>
                                            {messages['index.contact.us'] /* связаться с нами */}:
                                            <a className='nd_social_footer nd_social_footer_phone'    href="tel://8-800-550-14-38"/>
                                            <a className='nd_social_footer nd_social_footer_email'    href="mailto:sales@intenselife.ru"/>
                                            <a className='nd_social_footer nd_social_footer_vk'       href="https://vk.com/im?media=&sel=-148649037"/>
                                            <a className='nd_social_footer nd_social_footer_fb'       href="https://www.messenger.com/t/intenselife.ru"/>
                                            <a className='nd_social_footer nd_social_footer_telegram' href="https://t.me/Intenselife_bot"/>
                                            <div/>
                                            <div/>
                                        </div>
                                    </div>
                                    <div className='nd_footer_bottom'>
                                        <div className='nd_footer_content nd_space_between'>
                                            <div className="nd_logo" id="nd_logo_footer" onClick={()=> this.context.router.push( { pathname: '/' } )}></div>
                                            <div className='nd_footer_link' id="nd_omega_text"><a  onClick={()=>this.onPressMenuItem('static/official')}>{messages['index.omega']}</a></div>
                                            <div className='nd_footer_link' id="nd_cooperation_text">{messages['header.contacts.cooperation']}<br/>
                                                по тел: <a>+7-964-65-00000</a>,
                                                email: <a href='mailto:sales@intenselife.ru'>sales@intenselife.ru</a>
                                            </div>
                                            <div className='nd_footer_link'><a href="https://play.google.com/store/apps/details?id=ru.intenselife.mobile"><img src="/images/googleplay.png"/></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Dialog
                            title={messages['header.needauth.uppercase']}
                            actions={authActions}
                            open={this.state.openAuthDialog}
                            onRequestClose={() => this.handleAuthDialogClose}
                        >{messages['header.needauth.message']}
                        </Dialog>
                    </div>
                </MuiThemeProvider>
            </IntlProvider>);

    }
});

function mapStateToProps(state, ownProps) {
    return {
        inProgress:         state.auth.inProgress || state.catalog.inProgress || state.ribbon.inProgress,
        authISO:            state.authISO,
        isAuth:             state.authISO.sessionId && state.authISO.sessionId.length > 0 ? true : false,
        inProgressTotal:    state.auth.inProgressTotal,
        isUnauthorized:     state.auth.isUnauthorized,
        userData:           state.profiles.userData?state.profiles.userData:state.auth.userData,
        rehydrateComplete:  state.auth.rehydrateComplete,
        catalog:            state.catalog.catalog || [],
        subCategory:        state.catalog.subCategory || [],
        windowWidth:        state.sys.windowWidth,
        isServer:           state.authISO ? state.authISO.isServer : false,
        lang:               state.authISO ? state.authISO.lang : null
   }
}

export default connect(mapStateToProps, {
    doLogout:             AuthActions.doLogout,
    getCatalog:           CatalogActions.getCatalog,
    getAllDirectories:    DirectoriesActions.getAllDirectories,
    serviceFilter:        ServiceActions.serviceFilter,
    servicesPopular:      ServiceActions.servicesPopular,
    windowResizeHandler:  SysActions.windowResizeHandler,
    getInfoPage:          SysActions.getInfoPage
})(App);

