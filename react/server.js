"use strict";

import fs from 'fs';
import express  from 'express';
import cookieParser from 'cookie-parser';
import React    from 'react';
import ReactDom from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { Provider } from 'react-redux';
import {Helmet} from "react-helmet";
import Immutable from 'immutable';
import Serialize from 'remotedev-serialize';
const { stringify, parse } =  Serialize.immutable(Immutable);

import routes from './routes';
import configureStore from './store/configureStore';
import * as AuthISO from './actions/authActionsISO';
import * as ProfilesActions    from './actions/profilesActions';

import acceptLanguage from 'accept-language';

global.__DEV__ = process.env.NODE_ENV !== 'production';

const app = express();

app.use(cookieParser());

// статический контент
// будет обрабатывать webpack-dev-server либо nginx в продукции
app.use(express.static('build'));

app.use((req, res) => {
    const asyncTasks = []
    const store = configureStore({}, (result, action) => {
        console.log('push asyncTasks', action);
        asyncTasks.push(result);
    });



    // add headers
    res.append('Cache-Control' , 'no-cache, no-store, max-age=0'); // не кэшировать динамические страницы
    res.append('Expires', new Date().toUTCString());

    acceptLanguage.languages(['en-US', 'ru-RU']);
    
    store.dispatch(AuthISO.init(
        {
            cookies: req.cookies,
            verifyAction: ProfilesActions.getProfile(),
            isServer: true,
            lang: acceptLanguage.get(req.header('Accept-Language'))
        }))
        .then(() => match({ routes: routes(store), location: req.url }, (error, redirectLocation, renderProps) => {
            if (redirectLocation) { // Если необходимо сделать redirect
                return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
            }

            //console.log(2);
            if (error) { // Произошла ошибка любого рода
            // TODO: в production не стоит выводить детали ошибки
                return res.status(500).send(error.message);
            }

            if (!renderProps) { // Мы не определили путь, который бы подошел для URL
                console.log('2a');
                return res.status(404).send('Not found');
            }

            console.log(3);

            const componentHTML = ReactDom.renderToString(
                <Provider store={store}>
                    <RouterContext {...renderProps}/>
                </Provider>
            );

            console.log(4);

            const hostName = req.header('Host');

            if(asyncTasks.length > 0){
                // повторный рендеринг после выполнения всех асинхр. задач
                console.log('5a');

                let waitOneTask = () => {
                    if(asyncTasks.length > 0){
                        console.log('asyncTasks size: ' + asyncTasks.length);
                        let oneTask = asyncTasks.pop();
                        oneTask.then(() =>{
                           waitOneTask();
                        });
                    }else{
                        console.log('asyncTasks is empty');


                        renderProps.params.isSecondRender=true;
                        const componentHTML = ReactDom.renderToString(
                            <Provider store={store}>
                                <RouterContext {...renderProps}/>
                            </Provider>
                        );

                        res.end(renderHTML(componentHTML, store.getState(), hostName));
                    }

                };

                waitOneTask();

            }else{
                console.log('5b');
                return res.end(renderHTML(componentHTML, store.getState(), hostName));
            }
        }));

});



let bundleHash = '';

if(process.env.NODE_ENV == 'production'){
    // в продукции определим hash в именах бандлов
    try {
        const manifest = require('../build/webpack-manifest.json');

        Object.values(manifest).forEach(fileName => {
            let found = fileName.match(/^.*(-.*)\.js$/)
            if(found && found.length > 0){
                bundleHash = found[1];
            }
        })
    }catch(err){
        console.error(err);
    }
}

function renderHTML(componentHTML, initialState, hostName) {
    let assetUrl = '';

    const helmet = Helmet.renderStatic();

    const data = Immutable.fromJS(initialState);
    const serializedState = stringify(data);

    return `<!DOCTYPE html>
<html lang="ru">
  <head>
     <link rel="icon" href="https://static.intenselife.ru/il_launcher.png">
    <link rel="stylesheet" href="/mystyles.css">
    <link rel="stylesheet" href="/dropmenu.css">
    <link rel="stylesheet" href="/newstyles.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="https://npmcdn.com/react-selectize@2.0.2/dist/index.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="Description" content="Отдыхай активно">
     ${helmet.title.toString()}
     ${helmet.meta.toString()}
     ${helmet.link.toString()}
    <meta name="Keywords"    content="">
    <meta name="yandex-verification" content="5d7dae1e8f8cae94" />
    <title>IntenseLife - здесь можно купить, продать услуги в туризме и спорте</title>
    <script src="https://vk.com/js/api/openapi.js?146" type="text/javascript"></script>
    <script type="application/javascript">
       window.REDUX_INITIAL_STATE = ${serializedState};
    </script>
  </head>
  <body style="background:#e5e5e5 !important">
    <div id="root">${componentHTML}</div>
    <script type="application/javascript" src="${assetUrl}/js/vendor${bundleHash}.js"></script>
    <script type="application/javascript" src="${assetUrl}/js/app${bundleHash}.js"></script>
  </body>
</html>`;
}

const PORT = process.env.PORT || 8070;

app.listen(PORT, () => {
    console.log(`Server listening on: ${PORT} since ${new Date()}`);
});
