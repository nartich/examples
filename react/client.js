/**
 *
 */

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import {addLocaleData, IntlProvider,intlShape} from 'react-intl';
import { Router, browserHistory } from 'react-router';
import langMessages from './lang';
import Immutable from 'immutable';
import Serialize from 'remotedev-serialize';
const { parse } =  Serialize.immutable(Immutable);

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// project
import configureStore      from './store/configureStore.js';
import * as AuthActions    from './actions/authActions';
import * as AuthActionsISO    from './actions/authActionsISO';
import * as ProfilesActions    from './actions/profilesActions';
import ILApiService        from './services/ILApiService.js';

import routes from './routes';

import ru from 'react-intl/locale-data/ru';
addLocaleData([...ru]);
const defaultLocale = 'en';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

console.log('window.REDUX_INITIAL_STATE',  window.REDUX_INITIAL_STATE);

const store = configureStore( window.REDUX_INITIAL_STATE ? parse( JSON.stringify(window.REDUX_INITIAL_STATE)) : {});

if (!global.Intl) {
    global.Intl = require('intl');
}

function getLang()
{
    if (navigator.languages != undefined)
        return navigator.languages[0];
    else
        return navigator.language;
}


global.__DEV__ = process.env.NODE_ENV !== 'production';
// объект предоставляющий сервис получения/сохранения авторизационного токена для ILApiService
var authBackend = {

    getToken: function () {
        var state = store.getState();
        return state.auth && state.auth.authorizationToken;
    },

    setToken: function (token) {
        // в данном случае не надо, так как  authorizationToken вытаскивается из userData в редьюсере
    },

    setAuthFailed:  function () {
        store.dispatch( AuthActions.setAuthFailed () );
    },

    getLocales: function(){
         const systemLocale = getLang();
         return [systemLocale, defaultLocale];
    },


    onOtherStatus: function (result)  {

        if (result.isUnauthorized)
        {
            store.dispatch(AuthActions.doLogout().then( data => {
                
            } ) );
            this.context.router.push( { pathname: '/auth' } )
        }

        if(result.isNeedAuth || result.isUnauthorized){
            store.dispatch(AuthActions.setAuthFailed());
            
            if ( this.context && this.context.router ) this.context.router.push( { pathname: '/auth' } )
            else location.href = '/auth';
        }

        if ( result.isUnconfirmedProfile )
        {
            location.href = '/profile/needfill/'+encodeURIComponent(location.pathname);
        }

        if ( result.isNotFound === true || result.status == 'NOT_FOUND' || result.status == 'ERROR' &&  result.statusCode == 500  )
        {
           location.href = '/notfound';
        }
    }

};

ILApiService.setAuthBackend(authBackend);

store.dispatch(AuthActionsISO.init(
    {
        isServer: false,
        verifyAction: ProfilesActions.getProfile()
    }))
    .then(() => {
        ReactDOM.render(
            <Provider store={store}>
                <Router history={browserHistory}>{routes(store)}</Router>
            </Provider>,
            document.getElementById('root'));
    });


