import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from '../reducers/reducers';
import createLogger from 'redux-logger';
import catchPromise from '../middlewares/reduxCatchPromise';

export default function configureStore(initialState, catchPromiseCb) {

    var middlewares = [];

    middlewares.push(catchPromise(catchPromiseCb));
    middlewares.push(thunk);

    if(process.env.NODE_ENV !== 'production'){
    }

    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(...middlewares)
        )
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept(() => {
            console.log('configureStore accept callback');
            const nextRootReducer = require('../reducers/reducers.js');
            store.replaceReducer(nextRootReducer);
        })
    }

    return store
}