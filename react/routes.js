/**
 *
 */

'use strict';

import React from 'react';

import ConnectedApp        from './components/app';
// screens
import MainPage            from './screens/mainpage.js';
import Ribbon              from './screens/ribbon.js';
import SearchScreen        from './screens/searchpage.js';
import AdvancedSearch      from './screens/advsearchpage.js';
import CatalogScreen2      from './screens/catalog.js';
import AuthScreen          from './screens/auth.js';
import PassChangeScreen    from './screens/passchange.js';
import PhoneChangeScreen   from './screens/phonechange.js';
import UserProfile         from './screens/profile.js';
import ProfileViewScreen   from './screens/profileview.js';
import ServicesScreen      from './screens/services.js';
import ServiceViewScreen   from './screens/serviceview.js';
import SellerServices      from './screens/sellerservices.js';
import MyOrders            from './screens/myorders.js';
import Invoice             from './screens/invoice.js';
import FavoriteServices    from './screens/favservices.js';
import EditService         from './screens/editservice.js';
import EditBooking         from './screens/editbooking.js';
import SellerOrders        from './screens/sellerorders.js';
import InfoPage            from './screens/info.js';
import NotFound            from './screens/notfound.js';
import EditEventSettings   from './screens/editorders.js';
import EditPeriod          from './screens/editperiod.js';

import { Route, IndexRoute } from 'react-router';

function requireAuth(store, nextState, replace, cb) {

    let state = store.getState();

    console.log('state', state)

    if(!state.auth.userData){
        replace({
            pathname: '/auth',
            query: state.authISO.isServer?{'from':  (nextState.location.pathname)}:null,
            state: { nextPathname: nextState.location.pathname }
        });
    }
    cb();

}

export default function routes(store) {
    const requireAuthBound = requireAuth.bind(null, store);

    return (
            <Route path='/' component={ConnectedApp}>
                <IndexRoute component={MainPage}/>
                <Route path='auth' component={AuthScreen}/>
                <Route path='auth/:reg' component={AuthScreen}/>
                <Route path='oauth/:method/' component={AuthScreen}/>
                <Route path='search' component={SearchScreen}/>
                <Route path='advsearch' component={AdvancedSearch}/>
                <Route path='advsearch/:catid' component={AdvancedSearch}/>
                <Route path='searchstr/:str'    component={AdvancedSearch}/>
                <Route path='advsearch/:catid/:subcat' component={AdvancedSearch}/>
                <Route path='ribbon' component={Ribbon}/>
                <Route path='passchange' component={PassChangeScreen}/>
                <Route path='catalog' component={CatalogScreen2}/>
                <Route path='catalog/:catid' component={CatalogScreen2}/>
                <Route path='profile' component={UserProfile} onEnter={requireAuthBound}/>
                <Route path='profile/needfill/:url'  component={UserProfile}/>
                <Route path='email/confirm/:confkey' component={UserProfile}/>
                <Route path='profile/:id' component={ProfileViewScreen}/>
                <Route path='services/:subcatid' component={ServicesScreen}/>
                <Route path='service/:id' component={ServiceViewScreen}/>
                <Route path='sellerservices' component={SellerServices}/>
                <Route path='favorite' component={FavoriteServices}/>
                <Route path='editservice/:id' component={EditService}/>
                <Route path='editbooking/:id' component={EditBooking}/>
                <Route path='editperiod/:srv_id/:id' component={EditPeriod}/>
                <Route path='editorders/:id' component={EditEventSettings}/>
                <Route path='myorders' component={MyOrders}/>
                <Route path='sellerorders' component={SellerOrders}/>
                <Route path='sellerorders/:id' component={SellerOrders}/>
                <Route path='invoice/:id' component={MyOrders}/>
                <Route path='invoiceview/:id' component={Invoice}/>
                <Route path='editservice' component={EditService}/>
                <Route path='static/:page'   component={InfoPage}/>
                <Route path='phonechange'   component={PhoneChangeScreen}/>
                <Route path='notfound' component={NotFound}/>
            </Route>);
    //
}
